const express= require ('express');
const path  =require('path');
const app= express();
const bodyparser =require('body-parser');
const api =require('./server/api');


//Middleware
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());



// setting static Files
app.use(express.static(path.join(__dirname,'dist/bitcasas')))
app.use(express.static(path.join(__dirname,'src/assets')))
// Routes
app.use('/',api);
app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname,'dist/bitcasas/index.html'))
})


//port
app.listen(3500,()=>{debugger;
    console.log("port listening in 3000");
})