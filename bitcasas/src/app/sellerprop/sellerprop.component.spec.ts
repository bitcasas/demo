import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerpropComponent } from './sellerprop.component';

describe('SellerpropComponent', () => {
  let component: SellerpropComponent;
  let fixture: ComponentFixture<SellerpropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerpropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerpropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
